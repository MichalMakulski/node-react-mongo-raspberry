import React from 'react';

export const Home = (props) => {
    const h1Txt = props.route ? props.route.h1Txt : props.h1Txt;
    const h2Txt = props.route ? props.route.h2Txt : props.h2Txt;
    
    return <div className="c-hero u-letter-box--super u-centered">
          <h1 className="c-heading c-heading--xlarge u-window-box--none">{h1Txt}</h1>
          <h2 className="c-heading c-heading--small u-window-box--none c-text--quiet">{h2Txt}</h2>
    </div>
}