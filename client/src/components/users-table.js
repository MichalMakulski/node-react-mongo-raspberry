import React from 'react';
import { Link } from 'react-router';
import cache from '../cache';

const UserRow = ({name, id, email, isActive}) => {
    const badgeTxt = isActive ? 'online' : 'offline';
    const badgeActiveClass = isActive ? 'c-badge--success' : '';
    
    return <Link className="table-link" activeClassName="table-link__active" to={"/users/" + id} > 
        <div className="c-table__row c-table__row--clickable">
            <div className="c-table__cell">
                { name }
                <span className={'c-badge c-badge--rounded ' + badgeActiveClass}>{badgeTxt}</span>
            </div>
            <div className="c-table__cell">{ email }</div>
        </div>
    </Link>
}

export const UsersTable = React.createClass({

    getInitialState() {
        return {usersData: undefined}
    },
    componentWillMount() {
        if (cache.users) {
            this.setState({
                usersData: cache.users
            })
        } else {
            fetch('/api/users')
                .then(data => data.json())
                .then(json => {
                    cache.users = json;
                    this.setState({
                        usersData: json
                    })
                })
        }
    },
    render() {
        if(!this.state.usersData) {
            return <p>Loading users data...</p>
        } else {
            const users = this.state.usersData.map((user) => {
                return <UserRow
                key={user._id}
                id={user._id}
                name={user.name}
                email={user.email}
                balance={user.balance}
                isActive={user.isActive}
                />
            });

            return <div>
            <div className="c-table__row c-table__row--heading">
                <span className="c-table__cell">Name</span>
                <span className="c-table__cell">E-mail</span>
                </div>
                { users }
            { this.props.children }
        </div>
        }
    }
});