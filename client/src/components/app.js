import React from 'react';
import { Link } from 'react-router';

export const App = ({ children }) => {
    return <div>
        <header>
            <nav className="c-nav c-nav--inline c-nav--high">
              <Link className="c-nav__item" to="/">Home</Link>
              <Link className="c-nav__item" to="/users">Users</Link>
              <Link className="c-nav__item c-nav__item--right" to="/about">About</Link>
            </nav>
        </header>
        { children }
    </div>
}