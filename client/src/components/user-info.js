import React from 'react';
import cache from '../cache';

export const UserInfo = React.createClass({

    getInitialState() {
        return {
            userID: this.props.params.id,
            userInfo: undefined
        }
    },
    componentWillMount() {
        fetch(`/api/users?_id=${this.state.userID}`)
            .then(data => data.json())
            .then(json => {
                cache[this.props.params.id] = json[0];
                this.setState({
                    userInfo: json[0]
                })
            })
    },
    shouldComponentUpdate(nextProps, nextState) {

        if (this.state.userInfo) {
            return this.state.userInfo._id !== nextProps.params.id;
        }

        return true;
    },
    componentWillReceiveProps(nextProps) {
        if (cache[nextProps.params.id]){
            this.setState({
                userInfo: cache[nextProps.params.id]
            })
        } else {
            fetch(`/api/users?_id=${nextProps.params.id}`)
                .then(data => data.json())
                .then(json => {
                    cache[nextProps.params.id] = json[0];
                    this.setState({
                        userInfo: json[0]
                    })
                })
        }
    },
    render() {
        if(!this.state.userInfo) {
            return <p>Loading user info...</p>
        } else {
            const user = this.state.userInfo;
            const address = user.address.split(',');

            return <div className="c-card">
                <div className="c-card__item c-card__item--divider">{user.name}</div>
            <div className="c-card__item o-grid">
                <div className="avatar o-grid__cell o-grid__cell--center o-grid__cell--width-10">
                <img className="" src={user.picture} />
        </div>
            <div className="o-grid__cell o-grid__cell--width-90">
                <p className="c-paragraph">{user.about}</p>
            </div>
            </div>
            <div className="c-card__item">
                <address className="c-address">
                <span className="c-address__heading">Address</span>
                {address[0]}<br />
                {address[1]}<br />
                {address[2]}<br />
                {address[3]}
                </address>
                </div>
                <div className="c-card__item c-card__item--divider">REGISTERED: {user.registered}</div>
            </div>
        }
    }
});