import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import { App } from './components/app';
import { Home } from './components/home';
import { UsersTable } from './components/users-table';
import { UserInfo } from './components/user-info';


ReactDOM.render(
<Router history={ browserHistory }>
    <Route component={ App }>
        <Route path="/" component={ Home } h1Txt="Welcome" h2Txt="Click USERS to checkout users data"/>
        <Route path="/users" component={ UsersTable } >
            <Route path="/users/:id" component={ UserInfo } />
        </Route>
    </Route>
</Router>
, document.getElementById('app'));

function handleErr (err) {
    ReactDOM.render(<Home h1Txt="Sorry" h2Txt={err.message} />, document.getElementById('app'));
}