const createServer = require('http').createServer;
const fs = require('fs');
const URL = require('url');
const querystring = require('querystring');
const mongo = require('./database');
const dbUrl = require('./config').dbUrl;

module.exports = createServer((req, res) => {

    if (req.url.indexOf('js/') !== -1) {
        res.writeHead(200, {'content-type': 'text/javascript'});
        fs.createReadStream(`${__dirname}/client/build/js/main.js`, 'utf8').pipe(res);
        return;
    }
    if (req.url.indexOf('css/') !== -1) {
        res.writeHead(200, {'content-type': 'text/css'});
        fs.createReadStream(`${__dirname}/client/build/css/style.css`, 'utf8').pipe(res);
        return;
    }
    if (req.url.indexOf('/api/users') !== -1) {
        const query = querystring.parse(URL.parse(req.url).query);

        mongo
            .connect(dbUrl)
            .then(db => mongo.get(db, query))
            .then(data => {
                if (data.length !== 1) {
                    data = data.map(user => {
                        return {
                            name: user.name,
                            email: user.email,
                            _id: user._id,
                            isActive: user.isActive
                        }
                    });
                }

                res.writeHead(200, {'content-type': 'application/json'});
                res.write(JSON.stringify(data));
                res.end();
            })
            .catch(err => console.log(err));
        return;
    }
    
    res.writeHead(200, {'content-type': 'text/html'});
    fs.createReadStream(`${__dirname}/client/index.html`, 'utf8').pipe(res);
});