const MongoClient = require('mongodb').MongoClient;
const spawn = require('child_process').spawn;

module.exports = {
  connect: connect,
  get: get
};

function connect (dbUrl) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(dbUrl, function(err, db) {
      if (err) reject(err);
      spawn('python', ['/home/pi/mongo/python_scripts/leds_on.py']);
      resolve(db);
    });
  })
}

function get(db, query) {
  if (query === undefined) {
    query = {};
  }
  return new Promise((resolve, reject) => {
    const collection = db.collection('users');
    collection.find(query).toArray(function(err, docs) {
      if (err) reject(err);
      resolve(docs);
      db.close();
      spawn('python', ['/home/pi/mongo/python_scripts/leds_off.py']);
    });
  });
}